package fr.berniolles.darkian.sample;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(VertxExtension.class)
public class TestCrudPoll {

  class Context {
    Context(Vertx vertx, VertxTestContext testContext) {
      this.vertx = vertx;
      this.testContext = testContext;
    }

    public Vertx vertx;
    public VertxTestContext testContext;
  }

  private WebClient client;

  @BeforeEach
  void deploy_verticle(Vertx vertx, VertxTestContext testContext) {
    client = WebClient.create(vertx);
    vertx.deployVerticle(new MainVerticle(), testContext.succeeding(id -> testContext.completeNow()));
  }

  @Test
  void scenario(Vertx vertx, VertxTestContext testContext) throws Throwable {
    Context ctx = new Context(vertx, testContext);
    Test_01_checkIfEmptyAtStartup(ctx)
      .compose(this::Test_02_addPoll)
      .compose(this::Test_03_failAddPoll)
      .compose(this::Test_04_checkNotEmpty)
      .compose(this::Test_05_addOption1)
      .compose(this::Test_06_addOption2)
      .compose(this::Test_07_voteValid)
      .compose(this::Test_08_getResults)
      .compose(this::Test_09_voteInvalid)
      .compose(this::Test_10_voteBadPoll)
      .setHandler(onDone -> {
        if (onDone.succeeded()) {
          testContext.completeNow();
        } else {
          testContext.failNow(onDone.cause());
        }
      });
  }

  private Future<Context> Test_01_checkIfEmptyAtStartup(Context ctx) {
    Future<Context> future = Future.future();
    client
      .get(8080, "127.0.0.1", "/api/v1/polls")
      .as(BodyCodec.jsonObject())
      .send(ar -> {
        if (ar.succeeded()) {
          // Obtain response
          HttpResponse<JsonObject> response = ar.result();
          if (response.body().isEmpty()) {
            future.complete(ctx);
          } else {
            future.fail(new Exception("Unexpected response"));
          }
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_02_addPoll(Context ctx) {
    Future<Context> future = Future.future();
    client
      .put(8080, "127.0.0.1", "/api/v1/poll/Dog%20or%20Cat")
      .send(ar -> {
        if (ar.succeeded() && ar.result().statusCode() == 204) {
          future.complete();
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_03_failAddPoll(Context ctx) {
    Future<Context> future = Future.future();
    client
      .put(8080, "127.0.0.1", "/api/v1/poll/Dog%20or%20Cat")
      .send(ar -> {
        if (ar.succeeded() && ar.result().statusCode() == 400) {
          future.complete();
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_04_checkNotEmpty(Context ctx) {
    Future<Context> future = Future.future();
    client
      .get(8080, "127.0.0.1", "/api/v1/polls")
      .as(BodyCodec.jsonObject())
      .send(ar -> {
        if (ar.succeeded()) {
          // Obtain response
          HttpResponse<JsonObject> response = ar.result();
          if (response.body().isEmpty()) {
            future.fail(new Exception("Unexpected response"));
          } else {
            future.complete(ctx);
          }
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_05_addOption1(Context ctx) {
    Future<Context> future = Future.future();
    client
      .put(8080, "127.0.0.1", "/api/v1/poll/Dog%20or%20Cat/Dog")
      .send(ar -> {
        if (ar.succeeded() && ar.result().statusCode() == 204) {
          future.complete();
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_06_addOption2(Context ctx) {
    Future<Context> future = Future.future();
    client
      .put(8080, "127.0.0.1", "/api/v1/poll/Dog%20or%20Cat/Cat")
      .send(ar -> {
        if (ar.succeeded() && ar.result().statusCode() == 204) {
          future.complete();
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_07_voteValid(Context ctx) {
    Future<Context> future = Future.future();
    client
      .post(8080, "127.0.0.1", "/api/v1/poll/Dog%20or%20Cat/Dog")
      .send(ar -> {
        if (ar.succeeded() && ar.result().statusCode() == 204) {
          future.complete();
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_08_getResults(Context ctx) {
    Future<Context> future = Future.future();
    client
      .get(8080, "127.0.0.1", "/api/v1/poll/Dog%20or%20Cat")
      .as(BodyCodec.jsonObject())
      .send(ar -> {
        if (ar.succeeded()) {
          // Obtain response
          HttpResponse<JsonObject> response = ar.result();
          if (response.body().isEmpty()) {
            future.fail(new Exception("Unexpected response"));
          } else {
            future.complete(ctx);
          }
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_09_voteInvalid(Context ctx) {
    Future<Context> future = Future.future();
    client
      .post(8080, "127.0.0.1", "/api/v1/poll/Dog%20or%20Cat/Goldfish")
      .send(ar -> {
        if (ar.succeeded() && ar.result().statusCode() == 404) {
          future.complete();
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }

  private Future<Context> Test_10_voteBadPoll(Context ctx) {
    Future<Context> future = Future.future();
    client
      .post(8080, "127.0.0.1", "/api/v1/poll/Life%20or%20Death/Yes")
      .send(ar -> {
        if (ar.succeeded() && ar.result().statusCode() == 404) {
          future.complete();
        } else {
          future.fail(new Exception("Error in request"));
        }
      });
    return future;
  }
}
