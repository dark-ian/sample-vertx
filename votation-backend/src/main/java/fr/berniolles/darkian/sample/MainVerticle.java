package fr.berniolles.darkian.sample;

import fr.berniolles.darkian.sample.bo.Poll;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MainVerticle extends AbstractVerticle {

  private HashMap<String, Poll> polls;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    polls = new HashMap<>();
    HttpServer server = vertx.createHttpServer();

    int httpPort = config().getInteger("http.port", 8080);

    Router router = Router.router(vertx);
    Set<String> allowedHeaders = new HashSet<>();
    allowedHeaders.add("x-requested-with");
    allowedHeaders.add("Access-Control-Allow-Origin");
    allowedHeaders.add("origin");
    allowedHeaders.add("Content-Type");
    allowedHeaders.add("accept");
    allowedHeaders.add("X-PINGARUNER");

    Set<HttpMethod> allowedMethods = new HashSet<>();
    allowedMethods.add(HttpMethod.GET);
    allowedMethods.add(HttpMethod.POST);
    allowedMethods.add(HttpMethod.OPTIONS);
    allowedMethods.add(HttpMethod.DELETE);
    allowedMethods.add(HttpMethod.PATCH);
    allowedMethods.add(HttpMethod.PUT);
    router.route().handler(CorsHandler.create("*").allowedHeaders(allowedHeaders).allowedMethods(allowedMethods));

    router.route(HttpMethod.GET, "/api/v1/polls").handler(routingContext -> {
      JsonObject value = new JsonObject();
      polls.forEach((k, v) -> value.put(k, v.toJsonObject()));

      routingContext.response().end(value.encodePrettily());
    });

    router.route(HttpMethod.PUT, "/api/v1/poll/:pollName").handler(routingContext -> {
      String pollName = routingContext.request().getParam("pollName");
      if (polls.containsKey(pollName)) {
        routingContext.response().setStatusCode(400).end();
      } else {
        polls.put(pollName, new Poll());
        routingContext.response().setStatusCode(204).end();
      }
    });

    router.route(HttpMethod.PUT, "/api/v1/poll/:pollName/:optionName").handler(routingContext -> {
      String pollName = routingContext.request().getParam("pollName");
      String optionName = routingContext.request().getParam("optionName");
      if (!polls.containsKey(pollName)) {
        routingContext.response().setStatusCode(404).end();
      } else {
        if (!polls.get(pollName).addOption(optionName)) {
          routingContext.response().setStatusCode(400).end();
        } else {
          routingContext.response().setStatusCode(204).end();
        }
      }
    });

    router.route(HttpMethod.POST, "/api/v1/poll/:pollName/:optionName").handler(routingContext -> {
      String pollName = routingContext.request().getParam("pollName");
      String optionName = routingContext.request().getParam("optionName");
      if (!polls.containsKey(pollName)) {
        routingContext.response().setStatusCode(404).end();
      } else {
        if (!polls.get(pollName).voteFor(optionName)) {
          routingContext.response().setStatusCode(404).end();
        } else {
          routingContext.response().setStatusCode(204).end();
        }
      }
    });

    router.route(HttpMethod.GET, "/api/v1/poll/:pollName").handler(routingContext -> {
      String pollName = routingContext.request().getParam("pollName");
      if (!polls.containsKey(pollName)) {
        routingContext.response().setStatusCode(404).end();
      } else {
        routingContext.response().end(polls.get(pollName).toJsonObject().encodePrettily());
      }
    });

    server.requestHandler(router).listen(
      httpPort,
      http -> {
        if (http.succeeded()) {
          startPromise.complete();
          System.out.println("HTTP server started on port " + http.result().actualPort());
        } else {
          startPromise.fail(http.cause());
        }
      });
  }

  public static void main(String[] args) throws InterruptedException {
    BlockingQueue<AsyncResult<String>> q = new ArrayBlockingQueue<>(1);
    Vertx.vertx().deployVerticle(new MainVerticle(), q::offer);
    AsyncResult<String> result = q.take();
    if (result.failed()) {
      throw new RuntimeException(result.cause());
    }
  }
}
