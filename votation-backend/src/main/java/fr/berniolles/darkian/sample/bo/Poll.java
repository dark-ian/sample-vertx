package fr.berniolles.darkian.sample.bo;

import io.vertx.core.json.JsonObject;

import java.util.HashMap;

public class Poll {

  private static class MutableInt {
    int value = 0;

    public void increment() {
      ++value;
    }

    public int get() {
      return value;
    }
  }

  private HashMap<String, MutableInt> votes;

  public Poll() {
votes = new HashMap<>();
  }

  public boolean addOption(String option) {
    if (votes.containsKey(option)) {
      return false;
    } else {
      votes.put(option, new MutableInt());
      return true;
    }
  }

  public boolean voteFor(String option) {
    if (!votes.containsKey(option)) {
      return false;
    } else {
      votes.get(option).increment();
      return true;
    }
  }

  public JsonObject toJsonObject() {
    JsonObject value = new JsonObject();
    votes.forEach((k,v) -> value.put(k, v.get()));
    return value;
  }
}
