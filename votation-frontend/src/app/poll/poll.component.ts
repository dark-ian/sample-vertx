import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Poll} from '../poll';
import {BackendService} from '../backend.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgbAlert} from '@ng-bootstrap/ng-bootstrap/alert/alert';
import {faPlusSquare} from '@fortawesome/free-solid-svg-icons';
import {AddOptionModalComponent} from '../add-option-modal/add-option-modal.component';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.css']
})
export class PollComponent implements OnInit {

  faPlusSquare = faPlusSquare;

  private success = new Subject<string>();
  private fail = new Subject<string>();

  successMessage: string;
  failMessage: string;

  @Input() poll: Poll;
  @Output() dataChanged: EventEmitter<any> = new EventEmitter();

  closeResult: string;

  constructor(private backendService: BackendService, private modalService: NgbModal) {
  }

  ngOnInit() {
    this.success.subscribe((message) => this.successMessage = message);
    this.success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);

    this.fail.subscribe((message) => this.failMessage = message);
    this.fail.pipe(
      debounceTime(5000)
    ).subscribe(() => this.failMessage = null);
  }

  doVote(option) {
    this.backendService.postRequest('/api/v1/poll/' + this.poll.name + '/' + option)
      .then(() => {
        this.dataChanged.emit(null);
      })
      .catch(() => {
        this.fail.next(`${new Date()} - Message successfully changed.`);
      });
  }

  open() {
    this.modalService.open(AddOptionModalComponent, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.backendService.putRequest('/api/v1/poll/' + this.poll.name + '/' + result)
        .then(() => {
          this.success.next(`${new Date()} - Message successfully changed.`);
          this.dataChanged.emit(null);
        })
        .catch(() => {
          this.fail.next(`${new Date()} - Message successfully changed.`);
        });
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
