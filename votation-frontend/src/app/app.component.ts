import {Component, OnInit} from '@angular/core';
import {BackendService} from './backend.service';
import {Poll} from './poll';
import {faSync, faPlusSquare, faServer} from '@fortawesome/free-solid-svg-icons';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  serverUrl = 'http://localhost:8080';
  barClass = 'alert-danger';

  faSync = faSync;
  faPlusSquare = faPlusSquare;
  faServer = faServer;

  closeResult: string;
  polls: Poll[] = [];

  private debounce = new Subject<string>();

  constructor(private backendService: BackendService, private modalService: NgbModal) {
    // this.debounce.subscribe((message) => this.failMessage = message);
    this.debounce.pipe(
      debounceTime(500)
    ).subscribe((newUrl) => {
      this.backendService.dataUrl = this.serverUrl;
      this.backendService.getRemoteData()
        .then(polls => {
          this.polls = polls;
          this.barClass = 'alert-success';
        })
        .catch(() => {
          // On fail
          this.barClass = 'alert-danger';
        });
      });
  }

  ngOnInit() {
    this.onRefresh();
  }

  onDataChanged(event) {
    this.onRefresh();
  }

  onServerChanged(event) {
    this.debounce.next(this.serverUrl);
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.backendService.putRequest('/api/v1/poll/' + result)
        .then(() => {
          // this.success.next(`${new Date()} - Message successfully changed.`);
          this.onRefresh();
        })
        .catch(() => {
          // this.fail.next(`${new Date()} - Message successfully changed.`);
        });
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onClickMe() {
    this.backendService.getSampleData().then(() => this.onRefresh()).catch(cause => {
      console.log(cause);
    });
  }

  onRefresh() {
    this.backendService.getRemoteData()
      .then(polls => this.polls = polls);
  }
}
