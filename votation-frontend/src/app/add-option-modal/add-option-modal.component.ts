import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-option-modal',
  templateUrl: './add-option-modal.component.html',
  styleUrls: ['./add-option-modal.component.css']
})
export class AddOptionModalComponent implements OnInit {

  textValue = '';

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  onSave() {
    this.activeModal.close(this.textValue);
  }

}
