import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {PollComponent} from './poll/poll.component';
import {ListOfPollsComponent} from './list-of-polls/list-of-polls.component';

import {HttpClientModule} from '@angular/common/http';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AddOptionModalComponent} from './add-option-modal/add-option-modal.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PollComponent,
    ListOfPollsComponent,
    AddOptionModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    FontAwesomeModule
  ],
  entryComponents: [AddOptionModalComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
