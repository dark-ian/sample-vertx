import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Poll} from '../poll';

@Component({
  selector: 'app-list-of-polls',
  templateUrl: './list-of-polls.component.html',
  styleUrls: ['./list-of-polls.component.css']
})
export class ListOfPollsComponent implements OnInit {

  @Input() polls: Poll[];
  @Output() dataChanged: EventEmitter<any> = new EventEmitter();

  constructor() {
      }

  ngOnInit() {
    }

  onDataChanged(event) {
    this.dataChanged.emit(event);
  }

}
