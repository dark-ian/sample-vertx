import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfPollsComponent } from './list-of-polls.component';

describe('ListOfPollsComponent', () => {
  let component: ListOfPollsComponent;
  let fixture: ComponentFixture<ListOfPollsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfPollsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfPollsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
