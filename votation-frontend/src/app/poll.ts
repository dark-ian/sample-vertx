export class Option {
  name: string;
  count: number;
}

export class Poll {
  name: string;
  options: Map<string, number> = new Map<string, number>();

  constructor(name: string) {
    this.name = name;
  }

  addOption(name: string, count: number): Poll {
    this.options.set(name, count);
    return this;
  }
}
