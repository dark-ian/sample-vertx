import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Poll} from './poll';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  dataUrl = 'http://127.0.0.1:8080';

  constructor(private http: HttpClient) {
  }

  putRequest(url: string): Promise<null> {
    return new Promise<null>((resolve, reject) => {
      this.http.put(this.dataUrl + url, {},
        {observe: 'response'}).subscribe(resp => {
        if (resp.status < 400) {
          resolve();
        } else {
          reject();
        }
      });
    });
  }

  postRequest(url: string): Promise<null> {
    return new Promise<null>((resolve, reject) => {
      this.http.post(this.dataUrl + url, {},
        {observe: 'response'}).subscribe(resp => {
        if (resp.status < 400) {
          resolve();
        } else {
          reject();
        }
      });
    });
  }

  getSampleData(): Promise<null> {
    return this.putRequest('/api/v1/poll/Dog%20or%20Cat')
      .then(() => this.putRequest('/api/v1/poll/Dog%20or%20Cat/Dog'))
      .then(() => this.putRequest('/api/v1/poll/Dog%20or%20Cat/Cat'));
  }

  getRemoteData(): Promise<Poll[]> {
    return new Promise<Poll[]>((resolve, reject) => {
      this.http.get(this.dataUrl + '/api/v1/polls', {observe: 'response'})
        .subscribe(response => {
          const polls: Poll[] = [];
          Object.entries(response.body).forEach(entry => {
            const poll: Poll = new Poll(entry[0]);
            Object.entries(entry[1]).forEach(innerEntry => {
              poll.addOption(innerEntry[0], Number(innerEntry[1]));
            });
            polls.push(poll);
          });
          resolve(polls);
        });
    });
  }
}
